import sys

total = 0
results = list()

for line in sys.stdin.readlines():
    wordCount = line.split()
    results.append((
        wordCount[0],int(wordCount[1])
    ))
    total += int(wordCount[1])

for word,count in results:
    print(word.ljust(15),"\t[",sep='',end='')
    for i in range(0,count*100//total,5):
        print("*",sep='',end='')
    print("] ",count*100//total,"%",sep='')