import sys
import string

words = sys.stdin.read().split()
wordsCounted = dict()

for word in words:
    cleanWords = word.translate(str.maketrans('','',string.punctuation)).upper()
    if(len(cleanWords) > 0):
        wordsCounted[cleanWords] = wordsCounted.get(cleanWords, 0) + 1

unsortedList = list()
for key,value in wordsCounted.items():
    unsortedList.append((value,key))

sortedList = sorted(unsortedList, reverse=True)
for value,key in sortedList:
    print(key,value)